<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\User;

class AdminController extends Controller
{
    public function index(){
        $users = User::all();
        $movies = Movie::all();
        $logs = Log::all();
        return view('admin.panel', compact('users', 'movies', 'logs'));
    }


}
