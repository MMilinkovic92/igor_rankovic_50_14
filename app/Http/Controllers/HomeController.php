<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Models\Movie;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        //hello svete kapitalisticki
        $movies = Movie::search()->simplePaginate(3);
        $movies->appends(['search' => request('search')]);

        return view("pages.home", [
            'movies' => $movies
        ]);
    }

    public function contact()
    {
        return view('pages.contact-us');
    }

    public function sendMessage()
    {
        Mail::to('igor@yopmail.com')->send(new ContactUs(request()->except('_token')));
    }
}
