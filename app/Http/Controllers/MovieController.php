<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Movie;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MovieController extends Controller
{
    public function create()
    {
        return view('pages.blog-create');
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'image_path' => 'required|mimes:jpg,jpeg,png|max:1014',
            'rating' => 'required'
        ]);

        $movieBlog = new Movie(request()->except('_token'));
        $movieBlog->store();
    }

    public function show()
    {
        $id = request('id');

        $movie = Movie::with('comments', 'user')->find($id);

        return view('pages.single-movie', ['movie' => $movie]);
    }

    public function leaveComment()
    {
        return view('pages.leave-comment', ['id' => request('id')]);
    }

    public function postComment()
    {
        $id = request('id');
        $comment = new Comment(array_merge(request()->except('_token'), ['movie_id' => $id]));
        $comment->store();

        return redirect()->route('blog.show', $id);
    }

    public function getEdit()
    {
        $movie = Movie::find(\request('id'));
        return view('pages.edit-blog', ['movie' => $movie]);
    }

    public function postEdit()
    {
        $movie = Movie::find(request('id'));
        $movie->updateMovie(request()->all());
    }
}
