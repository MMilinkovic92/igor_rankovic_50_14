<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{

    /**
     * Return page for user register.
     *
     * @return View
     */
    public function register()
    {
        return view('pages.register');
    }

    /**
     * Store user.
     *
     * @return RedirectResponse
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required',
            'password-repeat' => 'required|same:password'
        ]);

        $data = request()->except('_token');

        $user = new User($data);
        $user->store();

        return redirect()->back()->with(['message' => 'success']);
    }

    /**
     * Returns login page.
     *
     * @return View
     */
    public function login()
    {
        return view('pages.login');
    }

    /**
     * Login user.
     *
     * @return RedirectResponse
     */
    public function postLogin()
    {
        request()->validate([
            'email' => 'required|email|exists:App\Models\User,email',
            'password' => 'required'
        ]);

        $user = User::where('email', request('email'))->first();

        if ($user->canLogin(request('password'))) {
            $user->login();
        }

        if($user->role_id==1){
            session(['user' => $user]);
            return redirect()->route('adminpanel');
        }
        else {
            return redirect()->route('home');
        }
    }

    /**
     * Logout user.
     *
     * @return RedirectResponse
     */
    public function postLogout()
    {
        if (request()->session()->exists('user')) {
            Log::LOG_ACTION('logged_out', session('user')->id);
            request()->session()->forget('user');
        }

        return redirect()->route('home');
    }
}
