<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'movie_id',
    ];

    public function store()
    {
        Log::LOG_ACTION('user_leave_comment', session('user')->id, $this->movie_id);
        $this->save();
    }
}
