<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $fillable = [
        'action',
        'user_id',
        'movie_id'
    ];

    public static function LOG_ACTION($action, $userId, $movieId = null)
    {
        Log::create([
            'action' => $action,
            'user_id' => $userId,
            'movie_id' => $movieId
        ]);
    }
}
