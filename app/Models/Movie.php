<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use function PHPUnit\Framework\throwException;

class Movie extends Model
{
    use HasFactory;

    /**
     * This attributes are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'short_description',
        'long_description',
        'image_path',
        'rating',
        'user_id'
    ];

    public function scopeSearch($query)
    {
        if ($query != "") return $query->where('title', 'like', "%" . request('search') . "%");
        return $query;
    }

    public function store()
    {
        $user = session('user');

        $this->image_path = $this->storeImage();
        $this->user_id = $user->id;

        $this->save();
        $this->fresh();

        Log::LOG_ACTION('create_movie', $user->id, $this->id);
    }

    private function storeImage()
    {
        if (request()->hasFile('image_path') && request()->file('image_path')->isValid()) {
            $imageName = Str::random(40) . Carbon::now()->timestamp;
            $extension = request()->image_path->extension();
            $imageName = $imageName . '.' . $extension;

            request()->image_path->storeAs('/images', $imageName, 'image');
        }

        return $imageName;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'movie_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function updateMovie($data)
    {
        if (request()->exists('image_path')) {
            $this->image_path = $this->storeImage();
        }
        $this->title = $data['title'];
        $this->short_description = $data['short_description'];
        $this->long_description = $data['long_description'];
        $this->rating = $data['rating'];
        $this->save();

        Log::LOG_ACTION('update_movie', session('user')->id, $this->id);
    }

    public function deleteMovie()
    {
        $this->comments()->delete();
        $this->delete();
    }
}
