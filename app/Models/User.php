<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use voku\helper\ASCII;
use function PHPUnit\Framework\isNull;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Store user.
     *
     * @param array $data
     */
    public function store()
    {
        $this->setPassword($this->password);
        $this->setDefaultRole();

        $this->save();
        $this->fresh();

        Log::LOG_ACTION('user_registered', $this->id);
    }

    /**
     * Check can User login.
     *
     * @param $password
     * @return bool
     */
    public function canLogin($password): bool
    {
        return $this->password == hash('sha256', $password);
    }

    /**
     * Set user to session.
     *
     * @return void
     */
    public function login(): void
    {
        if (!request()->session()->has('user')) {
            request()->session()->put('user', $this);
            Log::LOG_ACTION('user_login', $this->id);
        }

        Log::LOG_ACTION('already_logged', $this->id);
    }

    public function hasAdminPermission()
    {
        return $this->role->slug === "admin";
    }

    /**
     * Hash and set password.
     *
     * @param $password
     */
    private function setPassword($password)
    {
        $this->password = hash('sha256', $password);
    }

    /**
     * Set role to user.
     *
     * @param null $roleName
     */
    private function setDefaultRole($roleName = null)
    {
        $slug = isNull($roleName) ? "member" : $roleName;
        $this->role_id = Role::where('slug', $slug)->first()->id;
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function deleteUser()
    {
        $this->deleteLogsForUser();
        $this->deleteMoviesForUser();
        $this->delete();
    }

    public function deleteMoviesForUser()
    {
        $movies = Movie::where('user_id', $this->id);
        $moviesId = $movies->pluck('id');

        Comment::whereIn('movie_id', $moviesId)->delete();
        $movies->delete();
    }

    public function deleteLogsForUser()
    {
        Log::where('user_id', $this->id)->delete();
    }
}
