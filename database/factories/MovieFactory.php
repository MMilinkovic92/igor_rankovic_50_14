<?php

namespace Database\Factories;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $increment=1;
        return [
            'title' => $this->faker->unique()->name,
            'short_description' => $this->faker->randomAscii,
            'long_description' => $this->faker->randomAscii,
            'image_path' => "first_image_" . $increment++ .".jpg",
            'rating' => $this->faker->randomFloat(2, 1, 10)
        ];
    }
}
