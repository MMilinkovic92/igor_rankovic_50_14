<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Movie;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<100; $i++) {
            Comment::factory()->create([
                'movie_id' => rand(1, count(Movie::all()))
            ]);
        }
    }
}
