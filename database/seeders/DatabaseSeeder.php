<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleSeeder = new RoleSeeder();
        $roleSeeder->run();

        $userSeeder = new UserSeeder();
        $userSeeder->run();

        $movieSeeder = new MovieSeeder();
        $movieSeeder->run();

        $commentSeeder = new CommentSeeder();
        $commentSeeder->run();
    }
}
