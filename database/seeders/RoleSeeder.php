<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
           'name' => 'Admin',
           'description' => 'Admin desc',
           'slug' => 'admin'
        ]);

        Role::create([
            'name' => 'Member',
            'description' => 'Member desc',
            'slug' => 'member'
        ]);

        Role::create([
            'name' => 'Other',
            'description' => 'Other desc',
            'slug' => 'other'
        ]);
    }
}
