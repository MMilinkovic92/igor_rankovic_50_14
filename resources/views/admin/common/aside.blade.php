<aside class="main-sidebar sidebar-dark-primary elevation-4">
{{--            <!-- Brand Logo -->--}}
{{--            <a href="index3.html" class="brand-link">--}}
{{--                <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"--}}
{{--                     style="opacity: .8">--}}
{{--                <span class="brand-text font-weight-light">AdminLTE 3</span>--}}
{{--            </a>--}}

<!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">Igor Rankovic 50/14</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="/adminpanel" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/adminpanel/users" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/adminpanel/movies" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Movies</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/adminpanel/logs" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Logs</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
