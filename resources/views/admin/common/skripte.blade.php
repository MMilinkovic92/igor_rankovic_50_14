<!-- jQuery -->
<script src="{{asset('admin/jquery.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('admin/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
{{--<script src="plugins/chart.js/Chart.min.js"></script>--}}
<!-- Sparkline -->
{{--<script src="plugins/sparklines/sparkline.js"></script>--}}
<!-- JQVMap -->
{{--<script src="plugins/jqvmap/jquery.vmap.min.js"></script>--}}
{{--<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>--}}
<!-- jQuery Knob Chart -->
{{--<script src="plugins/jquery-knob/jquery.knob.min.js"></script>--}}
<!-- daterangepicker -->
{{--<script src="plugins/moment/moment.min.js"></script>--}}
{{--<script src="plugins/daterangepicker/daterangepicker.js"></script>--}}
<!-- Tempusdominus Bootstrap 4 -->
{{--<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>--}}
<!-- Summernote -->
{{--<script src="plugins/summernote/summernote-bs4.min.js"></script>--}}
<!-- overlayScrollbars -->
<script src="{{asset('admin/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/demo.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('/admin/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });

        $('.delete-object').submit(function (event){
            event.preventDefault();
            var id = this[2].value;
            var token = $('[name=_token]').val();
            var route = $('#url').val();
            route += "/" + id;

            $.ajax({
                type:'DELETE',
                url: route,
                data: {_token:token},
                success:function(response) {
                    alert(response.msg);
                    var element = "#object-" + id;
                    $(element).remove();
                }
            });
        });
    });


</script>
