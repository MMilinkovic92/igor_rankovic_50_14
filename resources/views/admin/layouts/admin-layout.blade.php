<!DOCTYPE html>
<html>
<head>
    @include('admin.common.head')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

{{-- Top menu navbar--}}
@include('admin.common.navbar')

<!-- Main Sidebar Container -->
@include('admin.common.aside')

@yield('content')

@include('admin.common.footer')

@include('admin.common.skripte')


</div>
<!-- ./wrapper -->
</body>
</html>
