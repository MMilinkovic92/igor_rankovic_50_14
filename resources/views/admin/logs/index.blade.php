@extends('admin.layouts.admin-layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-3">
                        <h1>Logs</h1>

                    </div>
                    <div class="col-sm-3">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-default-success">
                                <p>{{$message}}</p>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops! </strong> nije uspeo unos.<br>
                            <ul>
                                @foreach ($errors as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div><!-- /.container-fluid -->
        </section>
{{--{{dd($logs)}}--}}
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Action</th>
                                        <th>user_id</th>
                                        <th>Movie_id</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($logs as $c)
                                        <tr>
                                            <td>{{$c->id}}</td>
                                            <td>{{$c->action}}</td>
                                            <td>{{$c->user_id}}</td>
                                            <td>{{$c->movie_id}}</td>
                                            <td>
                                                <form action="{{ route('logs.destroy', $c->id) }}" method="DELETE">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" onclick=" return confirm('Da li sigurno brisete?')" title="Delete" class="btn btn-md btn-danger" >Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Action</th>
                                        <th>user_id</th>
                                        <th>Movie_id</th>
                                        <th>Options</th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
