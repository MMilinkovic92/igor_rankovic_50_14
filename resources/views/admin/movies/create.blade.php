@extends('admin.layouts.admin-layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    Add Movie
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops! </strong> nije uspeo unos.<br>
                            <ul>
                                @foreach ($errors as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form form-group" action="{{route('movies.store')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <label>Title
                                                    <input type="text" class="form-control" name="title" >
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Short description
                                                    <textarea  name="short_description" class="form-control" rows="5" cols="30"></textarea>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Long Description
                                                    <textarea  name="long_description" class="form-control" rows="5" cols="30"></textarea>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label> Rating
                                                    <input type="text" class="form-control" name="rating" >
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Image
                                                    <input type="file" name="image_path">
                                                </label>
                                            </div>

                                            @method('post')
                                            <a href="{{route('movies.index')}}" class="btn btn-success">Back</a>
                                            <input type="submit" value="Add" class="pull-right btn btn-primary">
                                        </form>

                                    </div>
                                    {{--        </form>--}}

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection

