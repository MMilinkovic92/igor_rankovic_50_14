@extends('admin.layouts.admin-layout')

@section('content')
            @include('admin.common.ajax', ['route' => url('/adminpanel/movies')])
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-3">
                        <h1>Movies</h1>

                    </div>

                    <div class="col-sm-3">
                        <a class="btn btn-sm btn-success" href="/adminpanel/movies/create">Add new</a>
                    </div>
                    <div class="col-sm-3">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-default-success">
                                <p>{{$message}}</p>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops! </strong> nije uspeo unos.<br>
                            <ul>
                                @foreach ($errors as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Short desc</th>
                                        <th>Long desc</th>
                                        <th>Rating</th>
                                        <th>User</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($movies as $c)
                                        <tr id="object-{{$c->id}}">
                                            <td>{{$c->id}}</td>
                                            <td>{{$c->title}}</td>
                                            <td>{{$c->short_description}}</td>
                                            <td>{{$c->long_description }}</td>
                                            <td>{{$c->rating }}</td>
                                            <td>{{$c->user_id }}</td>
                                            <td>
                                                <form method="DELETE" class="delete-object">
                                                <a class="btn btn-md btn-info" href="{{route('movies.show',$c->id)}}">Show</a>
                                                <a class="btn btn-md btn-warning" href="{{route('movies.edit',$c->id)}}">Edit</a>
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden"  value="{{$c->id}}" id="object-id">
                                                <button type="submit" onclick=" return confirm('Da li sigurno brisete?')" class="btn btn-md btn-danger" >Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Short desc</th>
                                        <th>Long desc</th>
                                        <th>Rating</th>
                                        <th>User</th>
                                        <th>Options</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
