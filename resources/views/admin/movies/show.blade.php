@extends('admin.layouts.admin-layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    Show Movie
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Title:</label>
                                                    <p>{{$movie->title}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Short description
                                                </label>
                                                <p>{{$movie->short_description}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Long Description</label>
                                                <p>{{$movie->long_description}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label> Rating </label>
                                                <p>{{$movie->rating}}</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Image</label>
                                                <div class="img">
                                                    <img src="{{asset('images/'.$movie->image_path)}}" style="width: 500px; height: 500px;"/>
                                                </div>
                                            </div>
                                            <a href="{{route('movies.index')}}" class="btn btn-success">Back</a>
                                            <a href="{{$movie->id}}/edit" class="btn btn-md btn-warning">Edit</a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
