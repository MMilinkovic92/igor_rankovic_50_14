@extends('admin.layouts.admin-layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    Add User
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops! </strong> nije uspeo unos.<br>
                            <ul>
                                @foreach ($errors as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form form-group" action="{{route('store-user')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label>Name
                                                    <input type="text" class="form-control" name="name" >
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Email
                                                    <input type="text" class="form-control" name="email" >
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Password
                                                    <input type="text" class="form-control" name="password" >
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Role</label> <br>
                                                <select name="role_id" id="role">
                                                    <option value="0">Select role...</option>
                                                    @foreach($roles AS $role)
                                                        <option value="{{$role->id}}"> {{$role->name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @method('post')
                                            <a href="{{route('users')}}" class="btn btn-success">Back</a>
                                            <input type="submit" value="Add" class="pull-right btn btn-primary">
                                        </form>

                                    </div>
                                    {{--        </form>--}}

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection

