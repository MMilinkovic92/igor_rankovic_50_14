@extends('admin.layouts.admin-layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    Add User
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops! </strong> nije uspeo unos.<br>
                            <ul>
                                @foreach ($errors as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form form-group" action="{{route('update-user',$user->id)}}" method="post">
                                            @csrf
                                            @method('post')
                                            <div class="form-group">
                                                <label>Name
                                                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Email
                                                    <input type="text" class="form-control" name="email" value="{{$user->email}}">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Password
                                                    <input type="text" class="form-control" name="password" value="{{$user->password}}">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Role</label> <br>
                                                <select name="role" id="role">
                                                    <option value="0">Select role...</option>
                                                    @foreach($roles AS $role)
                                                        <option value="{{$role->id}}" @if($role->id == $user->role_id) selected @endif> {{$role->name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <a href="{{route('users')}}" class="btn btn-success">Back</a>
                                            <input type="submit" value="Add" class="pull-right btn btn-primary">
                                        </form>

                                    </div>
                                    {{--        </form>--}}

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection

