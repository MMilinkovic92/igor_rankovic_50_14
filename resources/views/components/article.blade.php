
@foreach($movies AS $movie)
<div>
        <header class="entry-header">
            <h1 class="entry-title"><a href="{{route('blog.show', $movie->id)}}" rel="bookmark">{{$movie->title}}</a></h1>
            <div class="entry-meta">
                <span class="posted-on"><time class="entry-date published">{{$movie->created_at}}</time></span>
                <span class="comments-link"><a href="{{route('blog.leave.comment', $movie->id)}}">Leave a comment</a></span>
            </div>
            <div class="entry-thumbnail">
                <img style="width: 100px; height: 100px;" src="{{asset('images/' . $movie->image_path)}}" alt="">
            </div>
        </header>
        <div class="entry-summary">
            <p>
                {{$movie->short_description}} <a class="more-link" href="{{route('blog.show', $movie->id)}}">Read more</a>
            </p>
        </div>
        <footer class="entry-footer">
            <span class="cat-links">
                Rating <a href="#" rel="category tag"> {{$movie->rating}} </a>, <a href="#" rel="category tag">embed</a>, <a href="#" rel="category tag">media</a>
            </span>
        </footer>
 </div>
@endforeach
