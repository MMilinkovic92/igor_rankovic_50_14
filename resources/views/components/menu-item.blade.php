

<li><a href="{{route('home')}}">Home</a></li>
{{--<li><a href="{{route('about')}}">About</a></li>--}}

@if(session()->exists('user'))
    @if(session()->get('user')->hasAdminPermission())
        <li><a href="{{route('adminpanel')}}">Admin</a></li>
    @endif
    <li><a href="{{route('user.get.logout')}}">Logout</a></li>
    <li><a href="{{route('blog.create')}}">Create Blog</a></li>
    <li><a href="{{route('contact')}}">Contact</a></li>
@else
    <li><a href="{{route('user.create')}}">Register</a></li>
    <li><a href="{{route('user.get.login')}}">Login</a></li>
@endif
