<aside id="recent-posts-2" class="widget widget_recent_entries">
    <div style="visibility: hidden">
    <h4 class="widget-title">Recent Posts</h4>
    <ul>
        <li>
            <a href="blog-single.html">Somewhere in time</a>
        </li>
        <li>
            <a href="blog-single.html">Thanks for watching!</a>
        </li>
        <li>
            <a href="blog-single.html/">Who could have thought?</a>
        </li>
        <li>
            <a href="blog-single.html">Text Alignement</a>
        </li>
        <li>
            <a href="blog-single.html">HTML Tags and Formatting</a>
        </li>
    </ul>
    </div>
</aside>
