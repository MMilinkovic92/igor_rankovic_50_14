<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Moschino | Minimalist Free HTML Portfolio by WowThemes.net</title>
    <link rel='stylesheet' href={{asset('/css/woocommerce-layout.css')}} type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset('/css/woocommerce-smallscreen.css')}}' type='text/css' media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' href='{{asset('css/woocommerce.css')}}' type='text/css' media='all'/>
    <link rel='stylesheet' href={{asset('css/font-awesome.min.css')}} type='text/css' media='all'/>
    <link rel='stylesheet' href={{asset('css/style.css')}} type='text/css' media='all'/>
    <link rel='stylesheet' href={{asset('https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500,700%7CHerr+Von+Muellerhoff:400,500,700%7CQuattrocento+Sans:400,500,700')}} type='text/css' media='all'/>
    <link rel='stylesheet' href={{asset('css/easy-responsive-shortcodes.css')}} type='text/css' media='all'/>
</head>
<body class="blog">
<div id="page">
    <div class="container">
        @include('layout.structures.header')
    <!-- #masthead -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area column two-thirds">
                <main id="main" class="site-main" role="main">
                    <div class="grid bloggrid">

                        @section('content')
                        @show

                    </div>
                    <div class="clearfix"></div>
                    <nav class="pagination"></nav>
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->

            <div id="secondary" class="column third">
                <div id="sidebar-1" class="widget-area" role="complementary">

                    @include('components.about-me')

                    @include('components.recent-post')

                    @include('components.like-on-facebook')

                    @include('components.recent-comment')

                </div>
                <!-- .widget-area -->
            </div>
            <!-- #secondary -->
        </div>
        <!-- #content -->
    </div>
    <!-- .container -->
    @include('layout.structures.footer')
</div>
<!-- #page -->
<script src={{asset('js/jquery.js')}}></script>
<script src={{asset('js/plugins.js')}}></script>
<script src={{asset('js/scripts.js')}}></script>
<script src={{asset('js/masonry.pkgd.min.js')}}></script>
</body>
</html>
