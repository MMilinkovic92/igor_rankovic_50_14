<header id="masthead" class="site-header">
    @include('components.site-branding')
    @include('layout.structures.nav-bar')
</header>
