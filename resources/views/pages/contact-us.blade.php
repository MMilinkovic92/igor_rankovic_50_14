@extends('layout.base-layout')


@section('content')

    @if ($errors->any())
        <div class="alert alert-danger" style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (!empty($message))
        <div class="alert alert-danger" style="color: green">
            <ul>
                {{$message}}
            </ul>
        </div>
    @endif

    <form action="{{route('post.contact')}}" method="POST">
        @csrf
        <h1>Contact Us</h1>
        <p>Please fill in this form to contact us.</p>
        <hr>

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter Email" name="email" id="email" required><br><br>

        <label for="psw"><b>Text</b></label>
        <textarea name="message" id="message" rows="4" cols="50"></textarea>
        <hr>

        <button type="submit" class="registerbtn">Send message</button>
    </form>

@endsection
