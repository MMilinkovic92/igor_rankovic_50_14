@extends('layout.base-layout')


@section('content')

    @if ($errors->any())
        <div class="alert alert-danger" style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (!empty($message))
        <div class="alert alert-danger" style="color: green">
            <ul>
                {{$message}}
            </ul>
        </div>
    @endif

    <form action="{{route('blog.post.edit', $movie->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <h1>Edit Blog</h1>
            <p>Please fill in this form to edit an movie blog.</p>
            <hr>

            <label for="title"><b>Title</b></label>
            <input type="text" placeholder="Enter Title" name="title" id="title" value="{{$movie->title}}" required><br><br>

            <label for="short_description"><b>Short description</b></label>
            <input type="text" placeholder="Enter Short Description" name="short_description" value="{{$movie->short_description}}" id="short_description" required><br><br>

            <label for="long_description"><b>Long description</b></label>
            <input type="text" placeholder="Enter Long Description" name="long_description" value="{{$movie->long_description}}" id="long_description" required><br><br>

            <label for="image"><b>Image</b></label>
            <input type="file" name="image_path" id="image" value="Insert images">

            <label for="rating"><b>Rating</b></label>
            <select name="rating" id="rating" required>
                @for($i=1; $i<=10; $i++)
                    <option value="{{$i}}" @if($i == $movie->rating) selected @endif>{{$i}}</option>
                @endfor
            </select>
            <hr>

            <button type="submit" class="registerbtn">Register</button>
        </div>

        <div class="container signin">
            {{--Redirekcija za login page--}}
            <p>Already have an account? <a href="#">Sign in</a>.</p>
        </div>
    </form>

@endsection
