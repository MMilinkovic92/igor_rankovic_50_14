@extends('layout.base-layout')

@section('content')
    <div style="width: 100%; height: 30px; margin-bottom: 20px; text-align: center;">
        {{ $movies->links() }}
    </div>

    <div style="width: 100%; min-height: 500px;">
        @include('components.article', ['movies' => $movies])
    </div>
@endsection


