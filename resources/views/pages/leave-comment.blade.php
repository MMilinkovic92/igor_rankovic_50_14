@extends('layout.base-layout')


@section('content')

    @if ($errors->any())
        <div class="alert alert-danger" style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (!empty($message))
        <div class="alert alert-danger" style="color: green">
            <ul>
                {{$message}}
            </ul>
        </div>
    @endif

    <form action="{{route('blog.post.comment', $id)}}" method="POST">
        @csrf
        <h1>Leave comment</h1>
        <p>Please fill in this form to leave comment.</p>
        <hr>

        <label for="email"><b>Title</b></label>
        <input type="text" placeholder="Enter Title" name="title" id="title" required><br><br>

        <label for="psw"><b>Description</b></label>
        <textarea name="description" id="description" rows="4" cols="50"></textarea>
        <hr>

        <button type="submit" class="fa-send-o">Send comment</button>
    </form>

@endsection
