@extends('layout.base-layout')


@section('content')

    @if ($errors->any())
        <div class="alert alert-danger" style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (!empty($message))
        <div class="alert alert-danger" style="color: green">
            <ul>
                {{$message}}
            </ul>
        </div>
    @endif

    <form action="{{route('user.post.login')}}" method="POST">
        @csrf
            <h1>Register</h1>
            <p>Please fill in this form to create an account.</p>
            <hr>

            <label for="email"><b>Email</b></label>
            <input type="text" placeholder="Enter Email" name="email" id="email" required><br><br>

            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" id="password" required><br><br>
            <hr>

            <button type="submit" class="registerbtn">Login</button>
    </form>

@endsection
