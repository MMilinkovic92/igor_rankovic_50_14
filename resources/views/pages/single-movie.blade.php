@extends('layout.base-layout')


@section('content')

        <div class="container">
            <label for="comment">
                <a href="{{route('blog.leave.comment', $movie->id)}}">Leave comment</a>
            </label>

            @if(session('user')->id == $movie->user_id)
                <label for="comment">
                    <a href="{{route('blog.get.edit', $movie->id)}}">Edit blog</a>
                </label>
            @endif


            <h1>{{$movie->title}}</h1>
            <p>{{$movie->shor_description}}</p>
            <hr>

            <img style="width: 400px; height: 400px" src="{{asset('/images/' . $movie->image_path)}}" alt=""> <br>

            <label for="short_description">
                {{$movie->long_description}}
            </label> <br>

            <label for="rating">
                <b>Rating</b> {{$movie->rating}}
            </label> <br>
            <hr>
        </div>
        <div style="width: 100%; height: 30px; margin-bottom: 20px; text-align: center;">
            {{ $movie->comments()->paginate(2)->links() }}
        </div>
        @foreach($movie->comments()->paginate(2) AS $comment)
        <div class="container">
            {{--Redirekcija za login page--}}
            <label for="short_description">
                <b>Title</b> {{$comment->title}}
            </label> <br>
            <label for="rating">
                <b>Description</b> {{$comment->description}}
            </label> <br>
            <hr>
        </div>
        @endforeach
@endsection
