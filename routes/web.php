<?php

use App\Http\Controllers\admin\AdminUserController;
use App\Http\Controllers\admin\AdminMovieController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [HomeController::class, 'index'])->name('home');

Route::get("/users/login", [UserController::class, 'login'])->name('user.get.login');
Route::post('/users/login', [UserController::class, 'postLogin'])->name('user.post.login');
Route::get('/users/logout', [UserController::class, 'postLogout'])->name('user.get.logout');

Route::get("/users/register", [UserController::class, 'register'])->name('user.create');
Route::post("/users", [UserController::class, 'store'])->name('user.store');

Route::get('/blog/create', [MovieController::class, 'create'])->name('blog.create');
Route::post('/blog/create', [MovieController::class, 'store'])->name('blog.create');

Route::get('/blog/show/{id}', [MovieController::class, 'show'])->name('blog.show');
Route::get('/blog/{id}/comment', [MovieController::class, 'leaveComment'])->name('blog.leave.comment');
Route::post('/blog/{id}/post/comment', [MovieController::class, 'postComment'])->name('blog.post.comment');
Route::get('/blog/edit/{id}', [MovieController::class, 'getEdit'])->name('blog.get.edit');
Route::post('/blog/edit/{id}', [MovieController::class, 'postEdit'])->name('blog.post.edit');

Route::get('/contact-us', [HomeController::class, 'contact'])->name('contact');
Route::post('/message', [HomeController::class, 'sendMessage'])->name('post.contact');

//admin panel
Route::get('/adminpanel', [AdminController::class, 'index'])->name('adminpanel')->middleware('admin');

//crud users
Route::get('adminpanel/users', [AdminUserController::class, 'index'])->name('users')->middleware('admin');
Route::get('adminpanel/users/create', [AdminUserController::class, 'create'])->name('add-user')->middleware('admin');
Route::post('adminpanel/users/store', [AdminUserController::class, 'store'])->name('store-user')->middleware('admin');
Route::get('adminpanel/users/edit/{id}', [AdminUserController::class, 'edit'])->name('edit-user')->middleware('admin');
Route::post('adminpanel/users/update/{id}', [AdminUserController::class, 'update'])->name('update-user')->middleware('admin');
Route::delete('adminpanel/users/delete/{id}', [AdminUserController::class, 'destroy'])->name('delete-user')->middleware('admin');

// crud za filmove,
Route::resource('adminpanel/movies', 'admin\AdminMovieController')->middleware('admin');

//crud za logove
Route::resource('adminpanel/logs', 'admin\AdminLogController')->middleware('admin');
